
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/add_sale_order.dart';
import 'package:stc/group_list.dart';
import 'package:stc/invoice_detail.dart';
import 'package:stc/network.dart';

import 'constant.dart';



class Receipt extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Receipt> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List saleOrderData;


  @override
  void initState() {

    _getSaleOrder();
        super.initState();
      }
    
      @override
      Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
    
        
    
        return Scaffold(
          appBar: new AppBar(
            title: new Text("Receipt"),
          ),
          body: saleOrderData!=null? new ListView.builder(
            itemCount: saleOrderData.length,
              itemBuilder: (context , index){

              var item = saleOrderData[index];
                return Card(
                    child: new ListTile(

                      leading: new Text("Receipt No\n${item['R_RECTNO']}"),
                      title: new Text("Amount : ${item['R_TOTAMT']}"),
                      subtitle: new Text("Date : ${item['R_RECTDT']}"),
                    ));
              }
          ):
          new Center(
            child: new Text("No Receipt Found"),
          ),
         
        );
      }
    
      void _getSaleOrder() async {

    var res = await getReceipt(Constant.cid);

    if(res != null){

      saleOrderData = res;

    }

    setState(() {

    });


      }
}
