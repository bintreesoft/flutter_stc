
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/add_sale_order.dart';
import 'package:stc/group_list.dart';
import 'package:stc/network.dart';

import 'constant.dart';



class SaleOrder extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SaleOrder> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List saleOrderData;

  bool isCreate = true;
  @override
  void initState() {

    _getSaleOrder();
        super.initState();
      }
    
      @override
      Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
    
        
    
        return Scaffold(
          appBar: new AppBar(
            title: new Text("Sale Order"),
          ),
          body: saleOrderData!=null? new ListView.builder(
            itemCount: saleOrderData.length,
              itemBuilder: (context , index){

              var item = saleOrderData[index];
                return Card(
                    child: new ListTile(
                      leading: new Text("Invoice no\n${item['tr_InvoiceNo']}"),
                      title: new Text("Party Name : ${item['tr_PartyName']}"),
                      subtitle: new Text("Order Amount : ${item['tr_OrderAmount']}\n"
                          "Discount Amount : ${item['tr_TotalDiscount']}\n"
                          "Total Amount : ${item['tr_NetAmount']}\nInvoice Date : ${item['tr_InvoiceTime']}"),
                      onTap: (){
                        Constant.saleOrderId = item['tr_InvoiceNo'];
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => GroupList()),
                        );
                      },
                    ));
              }
          ):
          new Center(
            child: new Text("No Sale Order Found"),
          ),
          floatingActionButton: isCreate ? new FloatingActionButton.extended(
            onPressed: (){

              Constant.saleOrderId = "";
               Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => GroupList()),
          );
            },
            label: new Text("Create Sale Order"),
          ): new Container(),
        );
      }
    
      void _getSaleOrder() async {

        print(Constant.cid);
    List res = await getSaleOrder(Constant.cid);

    if(res != null && res.length > 0){

        isCreate = false;
      saleOrderData = res;

    }

    setState(() {

    });


      }
}
