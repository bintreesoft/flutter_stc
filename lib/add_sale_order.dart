import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/network.dart';
import 'package:stc/sale_order.dart';

import 'constant.dart';
import 'constant.dart';

class AddSaleOrder extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<AddSaleOrder> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List brandList;
  List itemList;

  List cart = new List();

  String selectedItemId = "0";
  var selectedItem;

  int expandIndex = 0;

  double totalAmount = 0.0;

  bool isLoading = false;

  @override
  void initState() {
    _getBrand();
    _getItem();
    _getSaleOrderDetail();
    super.initState();
  }

  _updateTotalAmount() {
    totalAmount = 0.0;
    if (cart.length > 0) {
      cart.forEach((item) {
//          double price = item['Qty'] > double.parse(item['packQty']) ? double.parse(item['packRate']) : double.parse(item['saleRate']) ;

        double price = double.parse(item['saleRate']);
        double per = double.parse(item['DiscountPer']);

        int qty = item['Qty'];

        var totalAmt = qty * price;
        var discountAmt = totalAmt * per / 100;

        var netAmt = totalAmt - discountAmt;

        totalAmount += netAmt;
      });
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        appBar: new AppBar(
          title: new Text("Sale Order"),
        ),
        body: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: new ListView.builder(
                      itemCount: itemList != null ? 1 : 0,
                      itemBuilder: (BuildContext context, int index) {
                        return new Card(
                            color:
                                selectedItemId == itemList[index]['mas_ItemId']
                                    ? Colors.pink
                                    : Colors.white,
                            child: ExpansionPanelList(
                              expansionCallback: (int index, bool isExpanded) {
                                itemList[index]['expanded'] = !isExpanded;

                                setState(() {});
                              },
                              children: itemList.map<ExpansionPanel>((item) {
                                return ExpansionPanel(
                                  headerBuilder:
                                      (BuildContext context, bool isExpanded) {
                                    return ListTile(
                                      title: Text("${item['unit']}"),
                                    );
                                  },
                                  body: new Column(
                                    children:
                                        item['items'].map<Widget>((brand) {
                                      return Card(
                                        color: brand['mas_ItemId'] ==
                                                selectedItemId
                                            ? Colors.redAccent
                                            : Colors.white,
                                        child: new ListTile(
                                          leading: new Text(
                                              "${brand['masItemSizeName']}"),
                                          trailing: new Image.network(
                                              "http://suryajtradingcompany.com/ItemPhoto/${brand['ItemImage']}"),
                                          title: new Text(
                                              "${brand['masItemBRANDName']}"),
                                          subtitle: new Text(
                                              "Sale Rate : ${brand['dt_Rrate']}\nMRP : ${brand['dt_MRP']}\nFlat Off  : ${((double.parse(brand['dt_MRP']) - double.parse(brand['dt_Rrate'])) * 100 / (double.parse(brand['dt_MRP']))).toStringAsFixed(2)} %"),
                                          onTap: () {


                                            if(int.parse(brand['qty']) <= 0) {
                                              print("item quantity is less than or eq to 0");
                                              return;
                                            }else{

                                              selectedItemId =
                                              brand['mas_ItemId'];

                                              int qty = 1;

                                              cart.forEach((i) {
                                                if ("${i['ItemId']}" ==
                                                    selectedItemId)
                                                  qty = i['Qty'];
                                              });
//
                                              selectedItem = {
                                                "ItemId": selectedItemId,
                                                "name": brand['masItemBRANDName'],
                                                "itemSize":
                                                brand['masItemSizeName'],
                                                "packRate": brand['dt_MRP'],
                                                "saleRate": brand['dt_Rrate'],
                                                "packQty": brand['mas_PackQty'],
                                                "DiscountPer":
                                                brand['mas_Discount'],
                                                "batchNo": brand['dt_BatchNo'],
                                                "Qty": qty
                                              };

                                            }



                                            setState(() {});
                                          },
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                  isExpanded: item['expanded'],
                                );
                              }).toList(),
                            ));
                      },
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 140,
              child: Card(
                child: new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: cart.length != 0 ? cart.length : 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (cart.length == 0) {
                      return Center(
                          child: new Text("Please add some item to cart"));
                    }

                    //print(cart[index]);

                    return Card(
                      color: Colors.greenAccent,
                      child: GestureDetector(
                        onTap: (){
                          print("tapped");
                          setState(() {
                            selectedItem = cart[index];
                            selectedItemId = cart[index]['ItemId'];
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text("${cart[index]['name']}"),
                              new Text("${cart[index]['itemSize']}"),
                              new Text("Qty : ${cart[index]['Qty']}"),
//                            new Text(
//                                "Price : ${cart[index]['Qty'] > double.parse(cart[index]['packQty']) ? cart[index]['packRate'] : cart[index]['saleRate']}"),
//                            new Text(
//                                "Total : ${cart[index]['Qty'] > double.parse(cart[index]['packQty']) ? double.parse(cart[index]['packRate']) * cart[index]['Qty'] : double.parse(cart[index]['saleRate']) * cart[index]['Qty']}"),
//
                              new Text("Price : ${cart[index]['saleRate']}/${cart[index]['packRate']}"),
                              new Text(
                                  "Total : ${double.parse(cart[index]['saleRate']) * cart[index]['Qty']}"),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            selectedItemId != "0"
                ? Card(
                    color: Colors.greenAccent,
                    child: new ListTile(
                      leading: new IconButton(
                        onPressed: () {
                          if (selectedItem['Qty'] <= 1) {
                            selectedItem['Qty'] = 0;
                          } else {
                            selectedItem['Qty'] -= 1;
                          }

                          setState(() {});
                        },
                        icon: Icon(Icons.remove),
                      ),
                      trailing: new IconButton(
                        onPressed: () {
                          selectedItem['Qty'] += 1;
                          setState(() {});
                        },
                        icon: Icon(Icons.add),
                      ),
                      title: new Text("Quantity ( ${selectedItem['Qty']} )"),
                      subtitle: new Text(
                        "BUY NOW",
                        style: new TextStyle(
                            color: Colors.green[900],
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                      onTap: () {
                        //print(cart);

                        //  print(selectedItemId);
                        List tempCart = new List();

                        cart.forEach((item) {
                          if ("${item['ItemId']}" != selectedItemId)
                            tempCart.add(item);
                        });

                        if (selectedItem["Qty"] > 0) tempCart.add(selectedItem);

                        cart = tempCart;

                        selectedItemId = "0";
                        selectedItem = null;

                        setState(() {});

                        _updateTotalAmount();
                      },
                    ),
                  )
                : new Container(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  new Text("Total Amount : ${totalAmount.toStringAsFixed(2)}"),
            ),
            new RaisedButton(
              onPressed: () async {
                if (Constant.isActive == "0") {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: new Text("Alert!"),
                        content: new Text(
                            "Your Profile is not active. You cannot create sale order."),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          new FlatButton(
                            child: new Text("Dismiss"),
                            onPressed: () {
                              Navigator.of(context).pop(1);
                            },
                          ),
                        ],
                      );
                    },
                  );

                  return;
                }

                if (isLoading) return;

                setState(() {
                  isLoading = true;
                });

                var totalAmount = 0.0;

//                {
//                  "ItemId": selectedItemId,
//                "name": itemList[index]['mas_ItemName'],
//                "packRate": itemList[index]['dt_PackRate'],
//                "saleRate": itemList[index]['dt_Rrate'],
//                "packQty": itemList[index]['mas_PackQty'],
//                "DiscountPer" : itemList[index]['mas_Discount'],
//
//                "Qty":qty
//                };
//

                if (cart.length > 0) {
                  List ji2 = new List();
                  if (cart.length > 0) {
                    cart.forEach((item) {
                      double price = item['Qty'] > double.parse(item['packQty'])
                          ? double.parse(item['packRate'])
                          : double.parse(item['saleRate']);
                      double per = double.parse(item['DiscountPer']);

                      int qty = item['Qty'];

                      var totalAmt = qty * price;
                      var discountAmt = totalAmt * per / 100;

                      var netAmt = totalAmt - discountAmt;

                      totalAmount += netAmt;

                      ji2.add({
                        "ItemId": "${item['ItemId']}",
                        "Qty": "${item['Qty']}",
                        "Rate": "${price}",
                        "DiscountPer": "${item['DiscountPer']}",
                        "Vat": "",
                        "Amount": "${totalAmt.toStringAsFixed(2)}",
                        "BatchNo": "${item['batchNo']}",
                        "VatId": "",
                        "SaleAccId": "",
                        "DiscAmt": "$discountAmt",
                        "NetAmt": "${netAmt.toStringAsFixed(2)}",
                        "VatAmt": "",
                        "TaxId": "",
                        "SGSTId": "",
                        "CGSTId": "",
                        "VatPer": "",
                        "OtherTaxId": "",
                        "OtherTaxPer": "",
                        "OtherTaxAmt": "",
                        "OtherTaxOn": ""
                      });
                    });
                  }

                  var data1 = [
                    {
                      "SaleOrderNo": Constant.saleOrderId != ""
                          ? Constant.saleOrderId
                          : "0",
                      "SaleOrderId": Constant.saleOrderId != ""
                          ? Constant.saleOrderId
                          : "0",
                      "OrderAmount": "$totalAmount",
                      "NetAmount": "$totalAmount",
                      "RoundOffAmt": "",
                      "UserID": "${Constant.cid}",
                      "Action": "0",
                      "CustomerName": "${Constant.name}",
                      "CustomerId": "${Constant.cid}"
                    }
                  ];

                  print(json.encode(data1));
                  print(json.encode(ji2));
                  var response =
                      await createOrder(json.encode(data1), json.encode(ji2));

                  print(response);
                  setState(() {
                    isLoading = false;
                  });

                  if (response != null && response == "1") {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return WillPopScope(
                          onWillPop: () {
                            print("will pop scope");

                            Navigator.of(context).pop(1);
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SaleOrder()),
                            );
                          },
                          child: AlertDialog(
                            title: new Text("Success"),
                            content: new Text("Sale Order Created"),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              new FlatButton(
                                child: new Text("OK"),
                                onPressed: () {
                                  Navigator.of(context).pop(1);
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SaleOrder()),
                                  );
                                },
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  } else {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Failed"),
                          content: new Text(
                              "Something went wrong! Please try again."),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("OK"),
                              onPressed: () {
                                Navigator.of(context).pop(1);
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }

                  //print(response);
                }
              },
              child: new Text("OK"),
              color: Colors.red,
            )
          ],
        ));
  }

  void _getItem() async {
    var res = await getItemList(Constant.gid);

    if (res != null) {
      itemList = res;

      List temp = new List();
//      itemList[index]['masItemSizeName']

      String unitName = "";
      itemList.map((item) {
        if (unitName == "" || unitName != item['masItemSizeName']) {
          unitName = item['masItemSizeName'];
          temp.add({"unit": unitName});
        }
      }).toList();

      List finalList = new List();
      temp.forEach((unit) {
        List product = new List();

        var un = unit['unit'];
        itemList.map((item) {
          if (un == item['masItemSizeName']) {
            product.add(item);
          }
        }).toList();

        finalList.add(
            {"unit": un, "expanded": finalList.length == 0, "items": product});
      });

      itemList = finalList;

      print(itemList);

      print(itemList.length);
    }

    setState(() {});
  }

  void _getBrand() async {
    setState(() {});
  }

  void _getSaleOrderDetail() async {
    if (Constant.saleOrderId != "") {
      List response =
          await getSaleOrderDetail(Constant.cid, Constant.saleOrderId);

      print(response.length);

      if (response != null && response.length > 0) {
        List items = new List();

        for (var item in response) {
          var itemId = item['tr_ItemId'];
          var groupId = item['mas_GroupId'];

          List groupedItem = await getItemList(groupId);

          groupedItem.forEach((i) {
            if (i['mas_ItemId'] == itemId) {
              var selectedItem = {
                "ItemId": itemId,
                "name": i['masItemBRANDName'],
                "itemSize": i['masItemSizeName'],
                "packRate": i['dt_MRP'],
                "saleRate": i['dt_Rrate'],
                "packQty": i['mas_PackQty'],
                "dtPackRate":i['dt_PackRate'],
                "DiscountPer": i['mas_Discount'],
                "batchNo": i['dt_BatchNo'],
                "Qty": double.parse(item['tr_Quantity']).toInt()
              };

              items.add(selectedItem);
            } else {}
          });
        }

//      print(items);

        cart = items;

        setState(() {});

        _updateTotalAmount();
      }
    }
  }
}
