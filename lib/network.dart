import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'constant.dart';





var url = "https://suryajtradingcompany.com/";








getLogin(user,pass) async {


  print('${url}Login_user.aspx?user=$user&pass=$pass');
  final response =
  await http.get('${url}Login_user.aspx?user=$user&pass=$pass');
  return json.decode(response.body);
}

getRegister(name,contact,address,email,pass,date) async {

  final response =
  await http.get('${url}Load_Customer_Insert.aspx?Name=$name&ContactNo=$contact&Address=$address&EmailId=$email&Password=$pass&RegDate=$date');
  return json.decode(response.body);
}


getProfileUpdate(cid,name,contact,address,email,pass,date, firmname, contactperson, gst, city, area, pincode) async {


    print('${url}Load_Customer_Insert.aspx?CustomerId=$cid&Name=$name&ContactNo=$contact&Address=$address&EmailId=$email&Password=$pass&RegDate=$date&UserId=$cid&FirmName=$firmname&ContactPerson=$contactperson&GSTNo=$gst&CityName=$city&Area=$area&PinCode=$pincode');
  final response =
  await http.get('${url}Load_Customer_Insert.aspx?CustomerId=$cid&Name=$name&ContactNo=$contact&Address=$address&EmailId=$email&Password=$pass&RegDate=$date&UserId=$cid&FirmName=$firmname&ContactPerson=$contactperson&GSTNo=$gst&CityName=$city&Area=$area&PinCode=$pincode');
  return json.decode(response.body);
}





getAccount() async {

  final response =
  await http.get('${url}Load_AccountLedger.aspx');
  return json.decode(response.body);
}




getUser() async {


  String userId = Constant.cid;

  print('${url}Load_UserList.aspx?UserId=$userId');
  final response =
  await http.get('${url}Load_UserList.aspx?UserId=$userId');
  return json.decode(response.body);
}


getBrand() async {

  final response =
  await http.get('${url}Load_BrandList.aspx');
  return json.decode(response.body);
}



getSize() async {

  final response =
  await http.get('${url}Load_SizeList.aspx');
  return json.decode(response.body);
}

getItemDept() async {

  final response =
  await http.get('${url}Load_ItemDeptList.aspx');
  return json.decode(response.body);
}


getItemUnit() async {

  final response =
  await http.get('${url}Load_ItemUnitList.aspx');
  return json.decode(response.body);
}

getItemList(groupid) async {


  final response =
  await http.get('${url}Load_ItemList.aspx?groupid=$groupid');
  return json.decode(response.body);
}

//http://suryajtradingcompany.com/ItemPhoto/0045770823.jpg

generateOtp(mobile) async {

  final response =
  await http.get('${url}LoginReg_Validate.aspx?mobileno=$mobile');
  return json.decode(response.body);
}



createOrder(data1,data2) async {

    print('${url}Load_SaleOrder_Insert.aspx?SaleOrder=$data1&SaleOrderDtl=$data2');
  final response =
//  await http.get('?SaleOrder=$data1&SaleOrderDtl=$data2');


  await http.post('${url}Load_SaleOrder_Insert.aspx', body: {"SaleOrder": data1, "SaleOrderDtl":data2 });

  print("error occured");
  print(response.body);
  return response.body;
}




getGroupList() async {

  final response =
  await http.get('${url}Load_ItemGroupList.aspx');
  return json.decode(response.body);
}




getSaleOrder(userid) async {

    print(userid);
  final response =
  await http.get('${url}Load_SaleOrder.aspx?userid=$userid');
  return json.decode(response.body);
}


getSaleOrderDetail(userid, saleOrderId) async {

//  userid = 1;
  final response =
  await http.get('${url}Load_SaleOrderDetail.aspx?userid=$userid&SOrderId=$saleOrderId');
  return json.decode(response.body);
}


// http://suryajtradingcompany.com/Load_Invoices.aspx?userid=1
getInvoices(userid) async {



  final response =
  await http.get('${url}Load_Invoices.aspx?userid=$userid');
  return json.decode(response.body);

}

// http://suryajtradingcompany.com/Load_InvoiceDetails.aspx?invoiceid=20

getInvoicesDetail(invoiceid) async {


    print('${url}Load_InvoiceDetails.aspx?invoiceid=$invoiceid');
  final response =
  await http.get('${url}Load_InvoiceDetails.aspx?invoiceid=$invoiceid');
  return json.decode(response.body);

}



//brand => item => (packrate, salerate ,mas_PackQty , qty > mas_PackQty ? packrate : salerate) 


getPartyLedger(userid) async {

  final response =
  await http.get('${url}Load_PartyLedger.aspx?userid=$userid');
  return json.decode(response.body);

}



getReceipt(userid) async {


  final response =
  await http.get('${url}Load_Receipt.aspx?userid=$userid');
  return json.decode(response.body);

}


checkVersion(versionCode) async {


  final response =
  await http.get('${url}Load_CheckUpdate.aspx?version=$versionCode');
  return json.decode(response.body);

}


generatePassword(mobile) async {

  final response =
  await http.get('${url}Load_ForgotPassword.aspx?mobile=$mobile');
  return json.decode(response.body);
}


validOtp(mobile) async {

  final response =
  await http.get('${url}Load_ValidateMobile.aspx?mobile=$mobile');
  return json.decode(response.body);
}