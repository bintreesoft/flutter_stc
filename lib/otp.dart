import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stc/dashboard.dart';
import 'package:stc/main.dart';
import 'package:stc/network.dart';

import 'constant.dart';
import 'dart:ui' as ui;


class OTP extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'STC',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Verify OTP'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController name = new TextEditingController();
  TextEditingController mobile = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController otp = new TextEditingController();


  bool isLoading=false;
  String error;

  bool isOtpVerify = false;



  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final mobileField = TextField(
      style: style,
      controller: mobile,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Mobile",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final otpField = TextField(
      style: style,
      controller: otp,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "OTP",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final nameField = TextField(
      style: style,
      controller: name,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Name",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final addressField = TextField(
      style: style,
      controller: address,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Address",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final emailField = TextField(
      style: style,
      controller: email,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final passField = TextField(
      style: style,
      controller: pass,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async{




          setState(() {
            isLoading = true;
            error = null;
          });



          if(otp.text != "1111"){
            setState(() {

              isLoading = false;
              error = "Invalid OTP";
            });

            return;
          }



          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );



          DateTime date = DateTime.now();
//          String selectedDate = DateFormat("MM/dd/y").format(date);
//
//          var response = await getRegister(name.text, mobile.text, address.text, email.text, pass.text,selectedDate);
//
//          if(response!=null && response == 0){
//
//            setState(() {
//              isLoading = false;
//              error = "Invalid Credentials";
//            });
//
//          }else{
//
//
//          }



        },
        child: Text("Verify",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


   final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    return Scaffold(
      body: Container(

        child: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new SizedBox(
                    height: 80,
                  ),
                  SizedBox(
                    height: 100.0,
                    child: ShaderMask(
                      blendMode: BlendMode.srcIn,
                      shaderCallback: (Rect bounds) {
                        return ui.Gradient.linear(
                          Offset(12.0, 24.0),
                          Offset(24.0, 4.0),
                          [
                            Colors.red,
                            Colors.blue,
                          ],
                        );
                      },
                      child: new Text("STC",style: new TextStyle(fontSize: 72.0,fontWeight: FontWeight.bold)),
                    ),
                  ),
                  SizedBox(height: 45.0),

                  new Text("Verify OTP"),
                  otpField,
                  SizedBox(height: 25.0),

                  isLoading?Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new CircularProgressIndicator(),
                  ):new Container(),
                  error!=null?new Text('$error'):new Container(),

                  SizedBox(
                    height: 35.0,
                  ),
                  loginButon,
                  SizedBox(
                    height: 15.0,
                  ),
                  registerButton
                ],
              ),
            ),
          ),
        ),
          ),
      )
    );
  }
}