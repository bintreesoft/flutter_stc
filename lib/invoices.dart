
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/add_sale_order.dart';
import 'package:stc/group_list.dart';
import 'package:stc/invoice_detail.dart';
import 'package:stc/network.dart';

import 'constant.dart';



class Invoices extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Invoices> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List saleOrderData;


  @override
  void initState() {

    _getSaleOrder();
        super.initState();
      }
    
      @override
      Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
    
        
    
        return Scaffold(
          appBar: new AppBar(
            title: new Text("Invoices"),
          ),
          body: saleOrderData!=null? new ListView.builder(
            itemCount: saleOrderData.length,
              itemBuilder: (context , index){

              var item = saleOrderData[index];
                return Card(
                    child: new ListTile(
                      onTap: (){

                        Constant.invoiceId = item['tr_InvoiceNo'];

                         Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => InvoiceDetail()),
                            );
                      },
                      leading: new Text("Invoice no\n${item['tr_InvoiceNo']}"),
                      title: new Text("Party Name : ${item['tr_PartyName']}"),
                      subtitle: new Text("Order Amount : ${item['tr_OrderAmount']}\n"
                          "Discount Amount : ${item['tr_TotalDiscount']}\nTotal Tax : ${(double.parse(item['tr_TotalVat']) + double.parse(item['tr_TotalCess'])).toStringAsFixed(2)}\nDelivery Charge : ${item['tr_DeliveryChg']}\n"

                          "Total Amount : ${item['tr_NetAmount']}\nInvoice Date : ${item['tr_InvoiceTime']}"),
                    ));
              }
          ):
          new Center(
            child: new Text("No Invoice Found"),
          ),
         
        );
      }
    
      void _getSaleOrder() async {

    var res = await getInvoices(Constant.cid);

    if(res != null){

      saleOrderData = res;

    }

    setState(() {

    });


      }
}
