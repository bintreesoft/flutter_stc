import 'package:flutter/material.dart';
import 'package:stc/constant.dart';
import 'package:stc/dashboard.dart';
import 'package:stc/network.dart';
import 'package:stc/profile.dart';
import 'package:stc/register.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:ui' as ui;

import 'constant.dart';
import 'forgot.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'STC',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'STC Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();
  bool isLoading=false;
  String error;

  bool isVersionChecked = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _checkUpdate();
  }
  @override
  Widget build(BuildContext context) {
 

    final emailField = TextField(
      style: style,
      controller: username,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Username",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      controller: password,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async{

          setState(() {
            isLoading = true;
            error = null;
          });
           
           if(username.text == ""){
              setState(() {
                
                isLoading = false;
                error = "Please enter valid username";
              });

              return;
           }

             if(password.text == ""){
              setState(() {
                
                isLoading = false;
                error = "Please enter valid password";
              });

              return;
           }

        var response = await getLogin(username.text, password.text);

        print(response);

        if(response.toString() == [{"error":"1","Disc":"Not Found"}].toString()){

            setState(() {
            isLoading = false;
              error = response[0]['Disc'];
          });
           
        }else{

          Constant.name = username.text;
          Constant.pass = password.text;
          Constant.cid = response[0]['MUS_UserId'];
          Constant.userId = response[0]['CustomerId'];
          Constant.isActive = response[0]['Approve'];
          Constant.firmName = response[0]['firmname'];
          Constant.notification = response[0]['notification'];
          Constant.isProfile = response[0]['Profile'];


          if(Constant.isProfile == "2"){
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Profile()),
            );

          }else{
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Dashboard()),
            );

          }

        }

        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold
            )),
      ),
    );


     final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => Register()),
          );
        },
        child: Text("Register",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold
            )),
      ),
    );



    return Scaffold(
      body: Container(

        child: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new SizedBox(
                    height: 80,
                  ),
                  SizedBox(
                    height: 100.0,
                    child: ShaderMask(
            blendMode: BlendMode.srcIn,
            shaderCallback: (Rect bounds) {
              return ui.Gradient.linear(
                Offset(12.0, 24.0),
                Offset(24.0, 4.0),
                [
                  Colors.red,
                  Colors.blue,
                ],
              );
            },
            child: new Text("STC",style: new TextStyle(fontSize: 72.0,fontWeight: FontWeight.bold)),
          ),
                  ),
                  SizedBox(height: 45.0),
                  emailField,
                  SizedBox(height: 25.0),
                  passwordField,
                  SizedBox(
                    height: 35.0,
                  ),

                  isLoading ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new CircularProgressIndicator(),
                  ):new Container(),
                                    error!=null?new Text('$error'):new Container(),

                  isVersionChecked ? loginButon : new Container(),
                  SizedBox(
                    height: 15.0,
                  ),
                  isVersionChecked ?   registerButton : new Container(),
                  
                  GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Forgot()),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new Text("Forgot Password ? "),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
          ),
      )
    );
  }

  void _checkUpdate() async{

    String version = "17";
    var response = await checkVersion(version);
    print("version check response ${response}");
    if(!response){
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Alert!"),
            content: new Text(
                "New Update Available for app. please download from play store."),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("OK"),
                onPressed: () async {


                    const url = 'https://play.google.com/store/apps/details?id=com.suryajtradingcompany.app';
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch $url';
                    }


                },
              ),

            ],
          );
        },
      );
    }else{

      isVersionChecked = true;
      setState(() {

      });
    }
  }
}