import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stc/dashboard.dart';
import 'package:stc/main.dart';
import 'package:stc/network.dart';

import 'constant.dart';




class Profile extends StatefulWidget {



  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Profile> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController name = new TextEditingController();
  TextEditingController mobile = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController pass = new TextEditingController();

  TextEditingController firmname = new TextEditingController();
  TextEditingController contactperson = new TextEditingController();
  TextEditingController gstno = new TextEditingController();
  TextEditingController cityname = new TextEditingController();
  TextEditingController area = new TextEditingController();
  TextEditingController pincode = new TextEditingController();




  bool isLoading=false;
  String error;


    @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getList();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final mobileField = TextField(
      style: style,
      controller: mobile,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Mobile",
          labelText: "Mobile",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final nameField = TextField(
      style: style,
      controller: name,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Name",
          labelText: "Name",

          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final addressField = TextField(
      style: style,
      controller: address,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Address",
          labelText: "Address",

          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final emailField = TextField(
      style: style,
      controller: email,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          labelText: "Email",

          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final passField = TextField(
      style: style,
      controller: pass,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          labelText: "Password",

          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );




    final firmName = TextField(
      style: style,
      controller: firmname,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Outlet/Firm/Shop Name",
          labelText: "Outlet/Firm/Shop Name",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final contactPerson = TextField(
      style: style,
      controller: contactperson,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Contact Person",
          labelText: "Contact Person",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final gstNo = TextField(
      style: style,
      controller: gstno,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "GST no",
          labelText: "GST no",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final cityName = TextField(
      style: style,
      controller: cityname,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "City Name",
          labelText: "City Name",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final areaField = TextField(
      style: style,
      controller: area,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Area",
          labelText: "Area",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final pinCode = TextField(
      style: style,
      controller: pincode,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Pin Code",
          labelText: "Pin Code",

          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );



    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async{

          setState(() {
            isLoading = true;
            error = null;
          });

          if(name.text == ""){
            setState(() {

              isLoading = false;
              error = "Please enter valid username";
            });

            return;
          }

          if(mobile.text == "" && mobile.text.length != 10){
            setState(() {

              isLoading = false;
              error = "Please enter valid mobile";
            });

            return;
          }
          if(address.text == ""){
            setState(() {

              isLoading = false;
              error = "Please enter valid address";
            });

            return;
          }
          if(pass.text == "" && pass.text.length < 6){
            setState(() {

              isLoading = false;
              error = "Please enter valid password (Min length 6)";
            });

            return;
          }

          if(firmname.text == "" ){
            setState(() {

              isLoading = false;
              error = "Please enter valid Outlet/Firm/Shopname";
            });

            return;
          }

          if(cityname.text == "" ){
            setState(() {

              isLoading = false;
              error = "Please enter valid City name";
            });

            return;
          }

          if(area.text == "" ){
            setState(() {

              isLoading = false;
              error = "Please enter valid Area";
            });

            return;
          }

          if(pincode.text == "" ){
            setState(() {

              isLoading = false;
              error = "Please enter valid Pincode";
            });

            return;
          }

          DateTime date = DateTime.now();
          String selectedDate = DateFormat("MM/dd/y").format(date);

          print(selectedDate);

          var response = await getProfileUpdate(Constant.userId, name.text, mobile.text, address.text, email.text, pass.text,selectedDate,
          firmname.text, contactperson.text,  gstno.text, cityname.text, area.text,  pincode.text);


          response = int.parse(response[0]['error']);
          if(response!=null && (response == 1 || response == 2)){

            setState(() {
              isLoading = false;
              error = "Something went wrong";
            });

          }else if (response == 3){
            setState(() {
              isLoading = false;
              error = "Please complete your profile to access app features.";
            });

          }else{

            Constant.pass = pass.text;
            Constant.name = name.text;



            setState(() {
              isLoading = false;
              error = "Profile Updated";
            });


            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Dashboard()),
            );



          }



        },
        child: Text("Update Profile",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


   final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    return Scaffold(
      appBar: new AppBar(
        title: new Text("Update Profile"),
      ),
      body: Container(

        child: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
               
                  SizedBox(height: 45.0),
                  nameField,
                  SizedBox(height: 25.0),
                  mobileField,

                  SizedBox(height: 25.0),
                  addressField,
                  SizedBox(height: 25.0),
                  emailField,
                  SizedBox(height: 25.0),
                  passField,

                  SizedBox(height: 25.0),
                  firmName,
                  SizedBox(height: 25.0),
                  contactPerson,
                  SizedBox(height: 25.0),
                  gstNo,
                  SizedBox(height: 25.0),
                  cityName,
                  SizedBox(height: 25.0),
                  areaField,
                  SizedBox(height: 25.0),
                  pinCode,


                  isLoading?Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new CircularProgressIndicator(),
                  ):new Container(),
                  error!=null?new Text('$error'):new Container(),

                  SizedBox(
                    height: 35.0,
                  ),
                  loginButon,

                ],
              ),
            ),
          ),
        ),
          ),
      )
    );
  }

  void _getList() async {


      List response = await getUser();

    

      if(response != null){

        response.forEach((item){

            if(item["Name"] == Constant.name && item["Password"] == Constant.pass){
              //Constant.cid = item["CustomerId"];

              setState(() {
                name.text = Constant.name;
                pass.text = Constant.pass;
                address.text = item["Address"];
                email.text = item["EmailID"];
                mobile.text = item["ContactNo"];




                firmname.text = item["FirmName"];
                contactperson.text = item["ContactPerson"];
                gstno.text = item["GSTNo"];
                cityname.text = item["CityName"];
                area.text = item["Area"];
                pincode.text = item["PinCode"];


              });
            }
        });
      }


  }



}