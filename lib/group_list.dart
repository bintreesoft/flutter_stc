import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/add_sale_order.dart';
import 'package:stc/constant.dart';
import 'package:stc/network.dart';



class GroupList extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<GroupList> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List saleOrderData;


  @override
  void initState() {

    _getSaleOrder();
        super.initState();
      }
    
      @override
      Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
    
        
    
        return Scaffold(
          appBar: new AppBar(
            title: new Text("Group List"),
          ),
          body: saleOrderData!=null? new ListView.builder(
            itemCount: saleOrderData.length,
              itemBuilder: (context,index){
            
              return new ListTile(
                title: new Text("${saleOrderData[index]['masItemGroupName']}"),
                onTap: (){
                  Constant.gid = saleOrderData[index]['masItemGroupId'];

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => AddSaleOrder()),
                  );
                },
              );
            
          }):
          new Center(
            child: new Text("No Group List Found"),
          ),
        
        );
      }
    
      void _getSaleOrder() async {
    
          saleOrderData = await getGroupList();
          
          setState(() {
            
          });
          

      }
}
