import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/network.dart';
import 'package:stc/sale_order.dart';

import 'constant.dart';

class InvoiceDetail extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<InvoiceDetail> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List brandList;
  List itemList;


  List cart = new List();

  String selectedItemId = "0";
  var selectedItem;

  int expandIndex = 0;


  double totalAmount= 0.0;

  @override
  void initState() {

    _getInvoiceDetail();
    super.initState();
  }


    _updateTotalAmount(){

      totalAmount = 0.0;
      if(cart.length > 0){
        cart.forEach((item){



          double price = item['Qty'] > double.parse(item['packQty']) ? double.parse(item['packRate']) : double.parse(item['saleRate']) ;
          double per = double.parse(item['DiscountPer']);

          int qty = item['Qty'];

          var totalAmt = qty*price;
          var discountAmt = totalAmt*per/100;

          var netAmt = totalAmt - discountAmt;

          totalAmount +=netAmt;




        });
      }


      setState(() {

      });

    }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: new AppBar(
          title: new Text("Invoice Detail"),
        ),
        body: new Column(
          children: <Widget>[
            new Expanded(
              child: new ListView.builder(
                itemCount: cart.length,
                itemBuilder: (context, index){

                  var brand = cart[index];
                                                      
                                return   Card(
                                      child: new ListTile(
                                        leading: new Text(
                                            "${brand['masItemSizeName']}"),
                                            trailing: new Image.network("http://suryajtradingcompany.com/ItemPhoto/${brand['ItemImage']}"),
                                        title:
                                        new Text("${brand['masItemBRANDName']}"),
                                        subtitle: new Text("Rate : ${brand['lc_rate']}"
                                            "\nQuantity : ${brand['lc_qty']}"
                                            "\nTotal Amount: ${brand['lc_amt']}"
                                            "\nTotal Amount: ${brand['lc_amt']}"
                                            "\nTotal Amount: ${brand['lc_amt']}"),
                                      
                                      ),
                                    );

                },
              ),
            )
          ],
        ),
       
        );
  }



  void _getInvoiceDetail() async {


  List res =  await getInvoicesDetail( Constant.invoiceId);

   if(res != null &&  res.length != 0){


     List items = new List();


    for (var item in res) {


      var itemId = item['tr_ItemId'];
        var groupId = item['mas_GroupId'];

        List groupedItem = await getItemList(groupId);


        groupedItem.forEach((i){

          if(i['mas_ItemId'] == itemId){

            i['lc_qty'] = item['tr_Quantity'];
                        i['lc_amt'] = item['tr_Amount'];
                                                i['lc_rate'] = item['tr_Rate'];


            items.add(i);
            
          }else{

          }
        });


          
        }

        print(items);
    
        cart = items;

        setState(() {
          
        });


   }


  }



  void _getItem() async {
    var res = await getItemList(Constant.gid);

    if (res != null) {
      itemList = res;



      List temp = new List();
//      itemList[index]['masItemSizeName']

      String unitName = "";
      itemList.map((item)  {

        if(unitName == "" || unitName != item['masItemSizeName']){

          unitName = item['masItemSizeName'];
          temp.add({"unit": unitName});

        }
      }).toList();



      List finalList = new List();
      temp.forEach((unit){
        List product = new List();

          var un = unit['unit'];
        itemList.map((item){

          if(un == item['masItemSizeName']){

            product.add(item);


          }
        }).toList();


        finalList.add({
          "unit": un,
          "expanded" : finalList.length == 0,
          "items": product

        });



      });


        itemList = finalList;

        print(itemList);

        print(itemList.length);


    }

    setState(() {});
  }


}
