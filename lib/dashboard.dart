import 'package:flutter/material.dart';
import 'package:stc/ledger.dart';
import 'package:stc/profile.dart';
import 'package:stc/receipt.dart';
import 'dart:ui' as ui;

import 'package:stc/sale_order.dart';

import 'constant.dart';
import 'invoices.dart';

class Dashboard extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'STC',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Dashboard'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);




  var items = ["Sale Order", "Profile","Payment List","Invoice List", "Ledger"];
  var icons = [Icons.note_add,Icons.person,Icons.payment,Icons.poll, Icons.book];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        body: Container(
      child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[

              new SizedBox(
                height: 20.0,
              ),
            Text(
              'SURYAJ DASHBOARD',
              textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline,
                        ),
                           new SizedBox(
                height: 50.0,
              ),
              Text('${Constant.firmName}'),
              Expanded(
                child: GridView.count(
                physics: BouncingScrollPhysics(),

                    // Create a grid with 2 columns. If you change the scrollDirection to
                    // horizontal, this produces 2 rows.
                    crossAxisCount: 2,
                    // Generate 100 widgets that display their index in the List.
                    children: List.generate(5, (index) {


                      return GestureDetector(
                        onTap: (){
                          switch(index){
                            case 0 :{
                              Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SaleOrder()),
          );
          break;
                            }
                            case 1 :{
                              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Profile()),
                              );
                              break;
                            }
                            case 2 :{
                              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Receipt()),
                              );
                              break;
                            }

                            case 3 :{
                              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Invoices()),

                              );
                              break;
                            }

                            case 4 :{
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Ledger()),

                              );
                              break;
                            }
                          }
                        },
                            child: new Column(
                          children: <Widget>[

                             ShaderMask(
            blendMode: BlendMode.srcIn,
            shaderCallback: (Rect bounds) {
                return ui.Gradient.linear(
                  Offset(8.0, 24.0),
                  Offset(24.0, 4.0),
                  [
                    Colors.red,
                    Colors.blue,
                  ],
                );
            },
            child:                         Icon(icons[index],size: 72.0,color: Colors.lightBlue,),

          ),

                            Text(
                              '${items[index]}',
                            style: Theme.of(context).textTheme.headline,
                          )
                          ],
                        ),
                      );
                    }),
                  ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text('${Constant.notification}'),
              )
            ],
          )),
    ));
  }
}
