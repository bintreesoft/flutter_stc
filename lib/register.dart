import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stc/dashboard.dart';
import 'package:stc/main.dart';
import 'package:stc/network.dart';

import 'constant.dart';
import 'dart:ui' as ui;

import 'otp.dart';


class Register extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'STC',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Register'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController name = new TextEditingController();
  TextEditingController mobile = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController otp = new TextEditingController();


  bool isLoading=false;
  String error;

  bool isOtpVerify = false;
  bool isOtpSent = false;

  String otpCode = "";

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final mobileField = TextField(
      style: style,
      controller: mobile,
      onChanged: (mobileStr){

        if(mobile.text.length == 10){
          _sendOtp();
        }
      },
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "Mobile",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final otpField = TextField(
      style: style,
      controller: otp,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "OTP",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final nameField = TextField(
      style: style,
      controller: name,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "Name",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final addressField = TextField(
      style: style,
      controller: address,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "Address",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final emailField = TextField(
      style: style,
      controller: email,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final passField = TextField(
      style: style,
      controller: pass,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );


    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding:   EdgeInsets.all(10.0),

      onPressed: () async{




          setState(() {
            isLoading = true;
            error = null;
          });

          if(name.text == ""){
            setState(() {

              isLoading = false;
              error = "Please enter valid username";
            });

            return;
          }

          if(mobile.text == "" || mobile.text.length != 10){
            setState(() {

              isLoading = false;
              error = "Please enter valid mobile";
            });

            return;
          }
          if(address.text == ""){
            setState(() {

              isLoading = false;
              error = "Please enter valid address";
            });

            return;
          }
          if(pass.text == "" || pass.text.length < 6){
            setState(() {

              isLoading = false;
              error = "Please enter valid password (Min length 6)";
            });

            return;
          }


          if(otpCode == "" || otpCode != "${otp.text}"){

            setState(() {

              isLoading = false;
              error = "Please enter valid OTP";
            });

            return;
          }


          DateTime date = DateTime.now();
          String selectedDate = DateFormat("MM/dd/y").format(date);

          var response = await getRegister(name.text, mobile.text, address.text, email.text, pass.text,selectedDate);

          if(response.toString() != [{"error":"0","Disc":"Update"}].toString()){

            setState(() {
              isLoading = false;
              error = response[0]['Disc'];
            });

          }else{

            Constant.name = name.text;
            Constant.pass = pass.text;




            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            );

          }



        },
        child: Text("Register",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


   final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding:  EdgeInsets.all(10.0),

     onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    return Scaffold(
      body: Container(

        child: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new SizedBox(
                    height: 40,
                  ),
                  SizedBox(
                    height: 60.0,
                    child: ShaderMask(
                      blendMode: BlendMode.srcIn,
                      shaderCallback: (Rect bounds) {
                        return ui.Gradient.linear(
                          Offset(12.0, 24.0),
                          Offset(24.0, 4.0),
                          [
                            Colors.red,
                            Colors.blue,
                          ],
                        );
                      },
                      child: new Text("STC",style: new TextStyle(fontSize: 50.0,fontWeight: FontWeight.bold)),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  nameField,
                  SizedBox(height: 10.0),
                  mobileField,
                  isOtpSent? otpField : new Container(),
                  SizedBox(height: 10.0),



                  addressField,
                  SizedBox(height: 10.0),
                  emailField,
                  SizedBox(height: 10.0),
                  passField,
                  isLoading?Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new CircularProgressIndicator(),
                  ):new Container(),
                  error!=null?new Text('$error'):new Container(),

                  SizedBox(
                    height: 20.0,
                  ),
                  loginButon,
                  SizedBox(
                    height: 10.0,
                  ),
                  registerButton
                ],
              ),
            ),
          ),
        ),
          ),
      )
    );
  }

  void _sendOtp() async {

    var response = await validOtp(mobile.text);

    setState(() {
      isOtpSent = true;
      otpCode = "$response";
    });

  }
}