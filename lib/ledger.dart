
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:stc/add_sale_order.dart';
import 'package:stc/group_list.dart';
import 'package:stc/invoice_detail.dart';
import 'package:stc/network.dart';

import 'constant.dart';



class Ledger extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Ledger> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  List saleOrderData;


  @override
  void initState() {

    _getSaleOrder();
        super.initState();
      }
    
      @override
      Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
    
        
    
        return Scaffold(
          appBar: new AppBar(
            title: new Text("Ledger"),
          ),
          body: saleOrderData!=null? new ListView.builder(
            itemCount: saleOrderData.length,
              itemBuilder: (context , index){

              var item = saleOrderData[index];
                return Card(
                    child: new ListTile(

                      title: new Text("Particular : ${item['Particular']}"),
                      subtitle: new Text("Credit : ${item['Credit']}\n"
                          "Debit : ${item['Debit']}"),
                    ));
              }
          ):
          new Center(
            child: new Text("No Ledger Found"),
          ),
         
        );
      }
    
      void _getSaleOrder() async {

    var res = await getPartyLedger(Constant.cid);

    if(res != null){

      saleOrderData = res;

    }

    setState(() {

    });


      }
}
